plugins {
    java
    id("maven-publish")
    id ("org.springframework.boot") version "2.3.1.RELEASE"
    id ("io.spring.dependency-management") version "1.0.9.RELEASE"
}

group = "org.loudsi.simulation"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven {
        url = uri("https://gitlab.com/api/v4/groups/loudsi-group/-/packages/maven")
        name = "GitLab"
    }
}

dependencies {
    implementation("org.loudsi:common-utils:1.0-SNAPSHOT")
    implementation("org.loudsi:simulation-api:1.0-SNAPSHOT")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.10.4")
    implementation("org.springframework.boot:spring-boot-starter")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
}
val CI_TOKEN: String ? by project

publishing {

    publications {
        create<MavenPublication>("maven") {
            from(components["java"])
        }
    }
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/19506780/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = CI_TOKEN
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}
