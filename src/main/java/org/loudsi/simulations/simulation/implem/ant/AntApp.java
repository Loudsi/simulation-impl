package org.loudsi.simulations.simulation.implem.ant;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AntApp implements CommandLineRunner {

    private final AntEngineRunner antEngineRunner;
    private final AntEngineTrainer antEngineTrainer;

    public AntApp(AntEngineRunner antEngineRunner, AntEngineTrainer antEngineTrainer) {
        this.antEngineRunner = antEngineRunner;
        this.antEngineTrainer = antEngineTrainer;
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(AntApp.class, args);
    }

    //access command line arguments
    @Override
    public void run(String... args) throws Exception {
        antEngineRunner.RunEngine();
        antEngineTrainer.RunTraining();
    }

}
