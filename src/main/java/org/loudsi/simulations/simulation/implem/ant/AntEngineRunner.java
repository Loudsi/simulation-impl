package org.loudsi.simulations.simulation.implem.ant;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.loudsi.simulation.api.config.ConfigLoader;
import org.loudsi.simulation.api.engine.BaseEngine;
import org.loudsi.simulations.simulation.implem.ant.config.AntEngineConfig;
import org.loudsi.simulations.simulation.implem.ant.context.AntContext;
import org.loudsi.simulations.simulation.implem.ant.manager.AntEngineManager;
import org.springframework.stereotype.Component;

@Component
public class AntEngineRunner {

    private final AntEngineManager antEngineManager;

    public AntEngineRunner(AntEngineManager antEngineManager) {
        this.antEngineManager = antEngineManager;
    }

    public void RunEngine() throws InterruptedException, JsonProcessingException {
        final BaseEngine<AntContext, AntEngineConfig> baseEngine = new BaseEngine<>(
                AntContextBuilder.buildAntContext(),
                ConfigLoader.loadFromResource("config/ant/config.json", AntEngineConfig.class),
                antEngineManager
        );

        Thread thread = new Thread(baseEngine);
        thread.start();

        Thread.sleep(5000);

        baseEngine.stop();
        final ObjectMapper objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
        System.out.println(objectMapper.writeValueAsString(baseEngine.getCopiedContext()));
    }
}
