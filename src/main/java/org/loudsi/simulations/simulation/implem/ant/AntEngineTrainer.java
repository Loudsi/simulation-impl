package org.loudsi.simulations.simulation.implem.ant;

import org.loudsi.simulation.api.engine.IEngine;
import org.loudsi.simulation.api.training.runner.RunsResult;
import org.loudsi.simulation.api.training.runner.parallele.ParallelTrainerRunner;
import org.loudsi.simulation.api.training.trainer.tree.FileTreeTrainerRepository;
import org.loudsi.simulation.api.training.trainer.tree.TreeBasedEngineTrainer;
import org.loudsi.simulations.simulation.implem.ant.config.AntConfigGenerator;
import org.loudsi.simulations.simulation.implem.ant.config.AntEngineConfig;
import org.loudsi.simulations.simulation.implem.ant.context.AntColony;
import org.loudsi.simulations.simulation.implem.ant.context.AntContext;
import org.loudsi.simulations.simulation.implem.ant.manager.AntEngineManager;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class AntEngineTrainer {

    private final AntEngineManager antEngineManager;
    private final AntConfigGenerator antConfigGenerator;

    public AntEngineTrainer(AntEngineManager antEngineManager, AntConfigGenerator antConfigGenerator) {
        this.antEngineManager = antEngineManager;
        this.antConfigGenerator = antConfigGenerator;
    }

    public void RunTraining() {
        final TreeBasedEngineTrainer<AntContext, AntEngineConfig> trainer = new TreeBasedEngineTrainer<>(
                antConfigGenerator,
                getParallelTrainerRunner(),
                new FileTreeTrainerRepository<>("C:\\Development\\sources\\simulation\\trainerSave"));

        final AntEngineConfig bestConfigFromTraining = trainer.getBestConfigFromTraining(AntContextBuilder.buildAntContext(), antEngineManager, 10, true);
    }

    private ParallelTrainerRunner<AntContext, AntEngineConfig> getParallelTrainerRunner() {
        return new ParallelTrainerRunner<>() {

            @Override
            protected long getAllowedRunTime() {
                return 1000;
            }

            @Override
            public RunsResult<AntEngineConfig> buildRunsResult(AntEngineConfig config, Collection<IEngine<AntContext, AntEngineConfig>> engines) {
                final double foodCount = engines
                        .stream()
                        .mapToInt(
                                engine -> engine.getCopiedContext()
                                        .getColonies()
                                        .stream()
                                        .mapToInt(AntColony::getColonyFood)
                                        .sum())
                        .sum();

                final double totalCycle = engines
                        .stream()
                        .mapToDouble(IEngine::getCycle)
                        .sum();

                final double score = (foodCount * foodCount) / totalCycle;
                return new RunsResult<>(config, score);
            }
        };
    }
}
