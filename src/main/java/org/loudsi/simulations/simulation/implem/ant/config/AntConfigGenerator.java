package org.loudsi.simulations.simulation.implem.ant.config;

import org.loudsi.simulations.common.RandomHelper;
import org.loudsi.simulation.api.config.IEngineConfigurationGenerator;
import org.loudsi.simulations.simulation.implem.ant.context.AntAction;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class AntConfigGenerator implements IEngineConfigurationGenerator<AntEngineConfig> {

    public AntEngineConfig generate(int interval) {
        HashMap<AntAction, BehaviorConfig> behaviorConfigHashMap = new HashMap<>();
        for (AntAction behaviorAction : AntAction.values()) {
            behaviorConfigHashMap.put(
                    behaviorAction,
                    new BehaviorConfig(
                            RandomHelper.randomDoubleInclusive(interval),
                            RandomHelper.randomDoubleInclusive(interval),
                            RandomHelper.randomDoubleInclusive(interval),
                            generateRandomMapOfDouble(interval))
            );
        }
        return new AntEngineConfig(
                behaviorConfigHashMap,
                generateRandomMapOfDouble(interval),
                generateRandomMapOfInt(interval),
                generateRandomMapOfInt(interval)
        );
    }

    public AntEngineConfig generateFrom(AntEngineConfig previousConfig, double interval) {
        HashMap<AntAction, BehaviorConfig> behaviorConfigHashMap = new HashMap<>();

        for (AntAction behaviorAction : AntAction.values()) {
            behaviorConfigHashMap.put(behaviorAction,
                    new BehaviorConfig(
                            previousConfig.getAntBehaviorConfig().get(behaviorAction).getToColonyWeight() + RandomHelper.randomDoubleInclusive(interval),
                            previousConfig.getAntBehaviorConfig().get(behaviorAction).getInertiaWeight() + RandomHelper.randomDoubleInclusive(interval),
                            previousConfig.getAntBehaviorConfig().get(behaviorAction).getRandomness() + RandomHelper.randomDoubleInclusive(interval),
                            generateFromRandomMapOfDouble(previousConfig.getAntBehaviorConfig().get(behaviorAction).getAntActionWeight(), interval)
                    )
            );
        }

        return new AntEngineConfig(
                behaviorConfigHashMap,
                generateFromRandomMapOfDouble(previousConfig.getAntScentDetectionRadiusConfig(), interval),
                generateFromRandomMapOfInt(previousConfig.getAntPheromoneDepositFrequencyConfig(), (int) (interval)),
                generateFromRandomMapOfInt(previousConfig.getAntPheromoneStrenghtConfig(), (int) (interval))
        );
    }

    private HashMap<AntAction, Double> generateRandomMapOfDouble(double interval) {
        HashMap<AntAction, Double> map = new HashMap<>();
        for (AntAction value : AntAction.values()) {
            map.put(value, RandomHelper.randomDoubleInclusive(interval));
        }
        return map;
    }

    private HashMap<AntAction, Double> generateFromRandomMapOfDouble(HashMap<AntAction, Double> previous, double interval) {
        HashMap<AntAction, Double> newMap = new HashMap<>();
        previous.forEach((key, value) -> newMap.put(key, value + RandomHelper.randomDoubleInclusive(interval)));
        return newMap;
    }

    private HashMap<AntAction, Integer> generateFromRandomMapOfInt(HashMap<AntAction, Integer> previous, int interval) {
        HashMap<AntAction, Integer> newMap = new HashMap<>();
        previous.forEach((key, value) -> newMap.put(key, value + RandomHelper.randomIntInclusive(interval)));
        return newMap;
    }

    private HashMap<AntAction, Integer> generateRandomMapOfInt(int interval) {
        HashMap<AntAction, Integer> map = new HashMap<>();
        for (AntAction value : AntAction.values()) {
            map.put(value, RandomHelper.randomIntInclusive(interval));
        }
        return map;
    }
}
