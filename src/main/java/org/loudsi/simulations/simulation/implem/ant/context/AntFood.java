package org.loudsi.simulations.simulation.implem.ant.context;

import org.loudsi.simulation.api.util.Position;

public class AntFood {

    private final Position position;

    public AntFood(Position position) {
        this.position = position;
    }

    public Position getPosition() {
        return position;
    }

}
