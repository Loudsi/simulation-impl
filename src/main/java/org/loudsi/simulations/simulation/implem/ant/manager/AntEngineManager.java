package org.loudsi.simulations.simulation.implem.ant.manager;

import org.loudsi.simulation.api.engine.IEngineManager;
import org.loudsi.simulations.simulation.implem.ant.config.AntEngineConfig;
import org.loudsi.simulations.simulation.implem.ant.context.Ant;
import org.loudsi.simulations.simulation.implem.ant.context.AntColony;
import org.loudsi.simulations.simulation.implem.ant.context.AntContext;
import org.loudsi.simulations.simulation.implem.ant.manager.sub.AntManager;
import org.loudsi.simulations.simulation.implem.ant.manager.sub.ColonyManager;
import org.loudsi.simulations.simulation.implem.ant.manager.sub.PheromonesManager;
import org.loudsi.simulations.simulation.api.engine.IEngineManager;
import org.springframework.stereotype.Component;

@Component
public class AntEngineManager implements IEngineManager<AntContext, AntEngineConfig> {

    private final AntManager antManager;
    private final PheromonesManager pheromonesManager;
    private final ColonyManager colonyManager;

    public AntEngineManager(AntManager antManager, PheromonesManager pheromonesManager, ColonyManager colonyManager) {
        this.antManager = antManager;
        this.pheromonesManager = pheromonesManager;
        this.colonyManager = colonyManager;
    }

    @Override
    public void updateContext(AntContext context, AntEngineConfig antConfig, int engineCycle) {
        for (AntColony colony : context.getColonies()) {
            colonyManager.produceAnt(colony,antConfig);
            pheromonesManager.agePheromone(colony.getPheromones());
        }

        for (AntColony colony : context.getColonies()) {
            for (Ant ant : colony.getAnts()) {
                antManager.antInteract(ant, colony, context, antConfig);
                antManager.makeAntDecideDirection(ant, colony,antConfig);
                antManager.moveAnt(ant);
                antManager.addAntPheromone(engineCycle, ant, colony.getPheromones());
            }
        }
    }

    @Override
    public boolean isGoalReached(AntContext context) {
        return false;
    }

    @Override
    public int getEngineTickInMs(int engineCycle) {
        return 10;
    }
}
