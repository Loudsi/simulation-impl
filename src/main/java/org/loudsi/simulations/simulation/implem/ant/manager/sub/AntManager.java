package org.loudsi.simulations.simulation.implem.ant.manager.sub;

import org.loudsi.simulations.common.Pair;
import org.loudsi.simulation.api.util.Position;
import org.loudsi.simulation.api.util.Vector;
import org.loudsi.simulations.simulation.implem.ant.config.AntEngineConfig;
import org.loudsi.simulations.simulation.implem.ant.config.BehaviorConfig;
import org.loudsi.simulation.implem.ant.context.*;
import org.loudsi.simulations.simulation.api.util.Position;
import org.loudsi.simulations.simulation.api.util.Vector;
import org.loudsi.simulations.simulation.implem.ant.context.*;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class AntManager {

    private final PositionManager positionManager;
    private final PheromonesManager pheromonesManager;

    public AntManager(PositionManager positionManager, PheromonesManager pheromonesManager) {
        this.positionManager = positionManager;
        this.pheromonesManager = pheromonesManager;
    }

    public void moveAnt(Ant ant) {
        final Position newPosition = positionManager.apply(ant.getPosition(), ant.getVector());
        if (positionManager.isPositionInTheWorld(newPosition)) {
            ant.setPosition(newPosition);
        }
    }

    public void addAntPheromone(int cycle, Ant ant, List<Pheromone> pheromones) {
        if (cycle % (Math.abs(ant.getPutPheromoneFrequencies()) + 1) == 0) {
            pheromones.add(new Pheromone(ant.getPosition().clone(), Math.abs(ant.getPheromoneStrength()), ant.getAntAction()));
        }
    }

    public void makeAntDecideDirection(Ant ant, AntColony colony, AntEngineConfig antEngineConfig) {
        Set<Pheromone> pheromones = pheromonesManager.getPheromonesAroundAnt(ant, colony.getPheromones(), ant.getScentDetectionRadius());
        final Map<AntAction, List<Pheromone>> pheromoneByType = pheromones.stream().collect(Collectors.groupingBy(Pheromone::getPheromoneType));
        final Map<AntAction, org.loudsi.simulations.simulation.api.util.Vector> vectorsByPheromone = calcPheromoneVector(ant, pheromoneByType);


        List<Pair<org.loudsi.simulations.simulation.api.util.Vector, Double>> vectorByInterest = new ArrayList<>();
        final AntAction antCurrentAction = ant.getAntAction();
        final BehaviorConfig config = antEngineConfig.getAntBehaviorConfig().get(antCurrentAction);
        if (config != null) {
            vectorByInterest.add(new Pair<>(org.loudsi.simulations.simulation.api.util.Vector.from(ant.getPosition(), colony.getPosition()), config.getToColonyWeight()));
            vectorByInterest.add(new Pair<>(ant.getVector(), config.getInertiaWeight()));
            vectorByInterest.add(new Pair<>(org.loudsi.simulations.simulation.api.util.Vector.createRandom(), config.getRandomness()));

            for (AntAction possibleActions : AntAction.values()) {
                final org.loudsi.simulations.simulation.api.util.Vector antActionPheromoneVector = vectorsByPheromone.get(possibleActions);
                if (antActionPheromoneVector != null) {
                    vectorByInterest.add(new Pair<>(antActionPheromoneVector, config.getAntActionWeight(possibleActions)));
                }
            }
            final org.loudsi.simulations.simulation.api.util.Vector finalVector = org.loudsi.simulations.simulation.api.util.Vector.resultingPondered(vectorByInterest).normalise();
            ant.setVector(finalVector);
        } else {
            //TODO add warn log
            System.out.println("No configuration found for ant action : " + antCurrentAction);
        }

    }

    private static Map<AntAction, org.loudsi.simulations.simulation.api.util.Vector> calcPheromoneVector(Ant ant, Map<AntAction, List<Pheromone>> pheromoneByType) {
        final Map<AntAction, org.loudsi.simulations.simulation.api.util.Vector> pheromoneVectors = new HashMap<>();
        for (Map.Entry<AntAction, List<Pheromone>> pheromoneTypeListEntry : pheromoneByType.entrySet()) {
            final List<Pair<org.loudsi.simulations.simulation.api.util.Vector, Double>> poundWithVector = new ArrayList<>();
            for (Pheromone pheromone : pheromoneTypeListEntry.getValue()) {
                poundWithVector.add(new Pair<>(org.loudsi.simulations.simulation.api.util.Vector.from(ant.getPosition(), pheromone.getPosition()), pheromone.getStrenght()));
            }
            final org.loudsi.simulations.simulation.api.util.Vector resultingVector = Vector.resultingPondered(poundWithVector);
            pheromoneVectors.put(pheromoneTypeListEntry.getKey(), resultingVector);
        }
        return pheromoneVectors;
    }


    public  void antInteract(Ant ant, AntColony colony, AntContext world, AntEngineConfig engineConfig) {
        switch (ant.getAntAction()) {
            case FIND_FOOD:
                final Optional<AntFood> first = world.getFood().stream().filter(f -> positionManager.isInside(f.getPosition(), ant.getPosition(), ant.getAntActionRadius())).findFirst();
                if (first.isPresent()) {
                    ant.getFood();
                    ant.setAntAction(AntAction.BRING_BACK_FOOD);
                }
                break;
            case BRING_BACK_FOOD:
                if (positionManager.isInside(colony.getPosition(), ant.getPosition(), ant.getAntActionRadius())) {
                    ant.dropFood();
                    colony.setColonyFood(colony.getColonyFood() + 1);
                    ant.setAntAction(AntAction.FIND_FOOD);
                }
                break;
            default:
                System.out.println("No Actions");
        }
    }
}
