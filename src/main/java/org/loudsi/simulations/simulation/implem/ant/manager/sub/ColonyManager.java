package org.loudsi.simulations.simulation.implem.ant.manager.sub;

import org.loudsi.simulation.api.util.Vector;
import org.loudsi.simulations.simulation.implem.ant.config.AntEngineConfig;
import org.loudsi.simulations.simulation.implem.ant.context.Ant;
import org.loudsi.simulations.simulation.implem.ant.context.AntAction;
import org.loudsi.simulations.simulation.implem.ant.context.AntColony;
import org.loudsi.simulations.simulation.api.util.Vector;
import org.springframework.stereotype.Component;

@Component
public class ColonyManager {

    public void produceAnt(AntColony colony, AntEngineConfig config) {
        if (colony.getAnts().size() < 40) {
            colony.getAnts().add(
                    new Ant(
                            "Ant: " + colony.getAnts().size(),
                            colony.getPosition().clone(),
                            Vector.createRandom().normalise(),
                            AntAction.FIND_FOOD,
                            config.getAntScentDetectionRadiusConfig(),
                            10,
                            config.getAntPheromoneDepositFrequencyConfig(),
                            config.getAntPheromoneStrenghtConfig())
            );
        }
    }
}
