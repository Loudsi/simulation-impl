package org.loudsi.simulations.simulation.implem.ant.manager.sub;


import org.loudsi.simulations.simulation.implem.ant.context.Ant;
import org.loudsi.simulations.simulation.implem.ant.context.Pheromone;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class PheromonesManager {

    private PositionManager positionManager;

    public PheromonesManager(PositionManager positionManager) {
        this.positionManager = positionManager;
    }

    public void agePheromone(List<Pheromone> pheromones) {
        pheromones.forEach(pheromone -> pheromone.setStrenght(pheromone.getStrenght() - 1));
        pheromones.removeIf(pheromone -> pheromone.getStrenght() <= 0);
    }

    //TODO this is where i can win some time
    public Set<Pheromone> getPheromonesAroundAnt(Ant ant, List<Pheromone> pheromones, double detectionRadius) {
        return pheromones.stream().filter(pheromone -> positionManager.isInside(pheromone.getPosition(), ant.getPosition(), detectionRadius)).collect(Collectors.toSet());
    }

}
