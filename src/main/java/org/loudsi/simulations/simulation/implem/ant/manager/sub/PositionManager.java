package org.loudsi.simulations.simulation.implem.ant.manager.sub;

import org.loudsi.simulation.api.util.Position;
import org.loudsi.simulation.api.util.Vector;
import org.loudsi.simulations.simulation.implem.ant.context.AntContext;
import org.loudsi.simulations.simulation.api.util.Position;
import org.loudsi.simulations.simulation.api.util.Vector;
import org.springframework.stereotype.Component;

@Component
public class PositionManager {

    public Position apply(Position initialPosition, Vector direction) {
        return new Position(initialPosition.getX() + direction.getX(), initialPosition.getY() + direction.getY());
    }

    public boolean isPositionInTheWorld(Position newPosition) {
        return !(newPosition.getX() < 0)
                && !(newPosition.getY() < 0)
                && !(newPosition.getX() > AntContext.WIDTH)
                && !(newPosition.getY() > AntContext.LENGTH);
    }

    public boolean isInside(Position pointPosition, Position circleCenter, double detectionRadius) {
        return Math.hypot(circleCenter.getX() - pointPosition.getX(), circleCenter.getY() - pointPosition.getY()) <= detectionRadius;
    }
}
